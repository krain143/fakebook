<?php 
    $host = "127.0.0.1";
    $username = "root";
    $password = "root";
    $dbname = "fakebook";

    //connect to database server
    $conn = new mysqli($host, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
?>