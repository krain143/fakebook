var h1s = document.getElementsByTagName('h1');
var loginEl = document.getElementsByClassName('login');
var signupEl = document.getElementsByClassName('signup');

var current = loginEl[0],
    hidden = signupEl[0],
    temp;

for (i in h1s) {
    h1s[i].onclick = function() {
        current.style.display = 'none';
        hidden.style.display = 'block';

        temp = current;
        current = hidden;
        hidden = temp;
    };
}