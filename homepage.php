<?php
    session_start();
    include 'connect.php';

    $sql = "SELECT * FROM users WHERE id = ".$_SESSION['userid'];
    $result = $conn->query($sql);

    $first_result = $result->num_rows ? mysqli_fetch_assoc($result) : array();

    $name = $first_result['firstname'].' '.$first_result['lastname'];
?>

<!doctype html>
<html>
    <head>
        <title>
            Fakebook - <?php echo $name ?>
        </title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <a href="logout.php">Logout</a>
        <h1>Hello <?php echo $name ?>, Welcome to Fakebook!!!</h1>
        <div class="user-status">
            <form action="status.php" method="POST">
                <textarea name="status" class="form-control"></textarea>
                <button type="submit">status</button>
            </form>

            <ul>
                <?php
                    $statusSql = "SELECT firstname, lastname, status, user_status.id AS status_id FROM user_status, users WHERE users.id = user_status.user_id ORDER BY user_status.id DESC";
                    $statuses = $conn->query($statusSql);
                    while ($status = $statuses->fetch_assoc()) {
                ?>
                    <li class="status">
                        <p>
                            <b>
                                <?php echo $status['firstname'].' '.$status['lastname'].': ' ?>
                            </b>
                            <i>
                                "<?php echo $status['status'] ?>"
                            </i>
                        </p>
                        <div>
                            <?php 
                                $userLikedSql = "SELECT * FROM users, likes WHERE users.id = likes.user_id AND likes.status_id = ".$status['status_id']." AND users.id = ".$_SESSION['userid'];
                                $liked = $conn->query($userLikedSql);
                                
                                if ($liked->num_rows > 0) {
                            ?>
                                <a href="unlike.php?statusid=<?php echo $status['status_id'] ?>">Unlike</a>
                            <?php } else { ?>
                                <a href="like.php?statusid=<?php echo $status['status_id'] ?>">Like</a>
                            <?php } ?>
                        </div>
                        <span class="likes">
                            <?php
                                $likeSql = "SELECT firstname, lastname FROM users, likes WHERE users.id = likes.user_id AND likes.status_id = ".$status['status_id'];
                                $likes = $conn->query($likeSql);
                                $nlikes = 0;
                                while ($like = $likes->fetch_assoc()) {
                                    $extension = ', ';
                                    if (++$nlikes >= $likes->num_rows) {
                                        $extension = ' likes this';
                                    }
                                    elseif ($nlikes == ($likes->num_rows - 1)) {
                                        $extension = ', and ';
                                    }
                                    echo $like['firstname'].' '.$like['lastname'].$extension;
                                }
                            ?>
                        </span>
                    </li>
                <?php
                    }

                    $conn->close();
                ?>
            </ul>
        </div>
    </body>
</html>